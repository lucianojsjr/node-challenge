// Declarations
let app;
let http;
let httpServer;
let express;
let middlewares;
let routes;
let Routes;
let databases;
let Databases;

// Requirements
http = require('http');
express = require('express');
Routes = require('./routes');
Databases = require('./models/index');

// Settings
app = express();
middlewares = require('./middlewares/index')(app);

routes = new Routes(app);
routes.setup();


// Start
httpServer = http.createServer(app);

databases = new Databases(app);
databases.init((err, data) => {
    httpServer.listen(process.env.PORT || 3000, function () {
        console.log('The Server is Running.');
    });
});
