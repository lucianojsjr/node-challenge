module.exports = function (app) {
    const usuarioController = require('./controllers/usuario')(app);
    const Utils = require('./utils/utils')();
    const Auth = require('./middlewares/auth')(app);

    function setup() {
        /**
         * Rotas de Usuário
         */
        app.get('/usuarios/:id', Auth.checkUserAuth, usuarioController.indexAction);
        app.post('/usuarios', usuarioController.createAction);
        app.post('/login', usuarioController.loginAction);

        app.use(Utils.notFoundHandler);
    }

    return {
        setup: setup
    };
};
