module.exports = function (app) {
    let sequelize;
    const Sequelize = require('sequelize');
    const config = require('../config/config')['development'];
    const tableOptions = {
        timestamps: true,
        createdAt: 'data_criacao',
        updatedAt: 'data_atualizacao'
    };

    function defineTables() {
        let Usuario;
        let Telefone;

        //User Table
        Usuario = sequelize.define('usuario', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            nome: Sequelize.STRING,
            email: {
                type: Sequelize.STRING,
                allowNull: false,
                validate: {
                    isEmail: true
                },
                unique: {
                    args: true,
                    msg: 'Email já existente.'
                }
            },
            senha: Sequelize.STRING,
            token: Sequelize.TEXT,
            ultimo_login: {
                type: Sequelize.DATE,
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
            }
        }, tableOptions);

        //Phone Table
        Telefone = sequelize.define('telefone', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            numero: Sequelize.STRING,
            ddd: Sequelize.STRING
        }, tableOptions);

        Telefone.belongsTo(Usuario, {
            foreignKey: 'id_usuario'
        });

        Usuario.hasMany(Telefone, {
            foreignKey: 'id_usuario'
        });

        // Syncronize
        sequelize.sync();

        app.set('models', {
            Usuario: Usuario,
            Telefone: Telefone,
            Sequelize: Sequelize
        });
    }

    function init(callback) {
        let auth;

        if (process.env.DATABASE_URL) {
            sequelize = new Sequelize(process.env.DATABASE_URL, config.heroku_options);
        } else {
            sequelize = new Sequelize(config.database, config.username, config.password, config.options);
        }

        auth = sequelize.authenticate();

        auth.then(function () {
            console.log('Database is Connected');
            defineTables();
            return callback(null, 'Database is Connected');
        }).catch(function (err) {
            console.error('Database error: ', err);
            callback(err, null);
        });
    }

    return {
        init: init
    };
};
