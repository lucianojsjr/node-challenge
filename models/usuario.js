module.exports = function (app) {
    const async = require('async');
    const AuthUtils = require('../utils/auth-utils');

    function add(user, callback) {
        const Usuario = app.get('models').Usuario;
        const Telefone = app.get('models').Telefone;

        user.senha = AuthUtils.encryptPassword(user.senha);

        async.auto({
            create: function (done) {
                Usuario.create(user, {
                    include: [Telefone]
                }).then(result => done(null, result))
                    .catch(error => done(error, null));
            },
            generateToken: ['create', function (results, done) {
                results.create = JSON.parse(JSON.stringify(results.create));
                results.create.token = AuthUtils.getToken(results.create.id);

                delete results.create.senha;

                done(null, results.create);
            }],
            updateToken: ['generateToken', function (results, done) {
                Usuario.update({
                    token: results.generateToken.token
                }, {
                    where: {
                        id: results.generateToken.id
                    }
                }).then(result => done(null, result))
                    .catch(error => done(error, null));
            }]
        }, (err, results) =>
            callback(err, results.generateToken));
    }

    function find(id, callback) {
        const Usuario = app.get('models').Usuario;
        const Telefone = app.get('models').Telefone;

        Usuario.find({
            where: {
                id: id
            },
            include: [{
                model: Telefone
            }],
            attributes: {
                exclude: ['senha']
            }
        }).then(user => callback(null, user))
            .catch(error => callback(error, null));
    }

    function login(user, callback) {
        const Usuario = app.get('models').Usuario;
        const Telefone = app.get('models').Telefone;

        async.auto({
            user: function (done) {
                Usuario.find({
                    where: {
                        email: user.email
                    },
                    include: [{
                        model: Telefone
                    }]
                }).then(user => done(null, user))
                    .catch(error => done(error, null));
            },
            comparePassword: ['user', function (results, done) {
                let isSamePassword = false;

                results = JSON.parse(JSON.stringify(results));
                if (!results.user) {
                    return done({
                        status: 401,
                        message: 'Email e/ou senha inválidos'
                    }, null);
                }

                isSamePassword = AuthUtils.isValidPassword(user.senha, results.user.senha);

                if (!isSamePassword) {
                    return done({
                        status: 401,
                        message: 'Email e/ou senha inválidos'
                    }, null);
                }

                delete results.user.senha;

                done(null, results.user);
            }],
            updateLastLogin: ['comparePassword', function (results, done) {
                results = JSON.parse(JSON.stringify(results));
                results.comparePassword.ultimo_login = Date.now();
                results.comparePassword.token = AuthUtils.getToken(results.comparePassword.id, results.comparePassword.ultimo_login);

                Usuario.update({
                    token: results.comparePassword.token,
                    ultimo_login: results.comparePassword.ultimo_login
                }, {
                    where: {
                        id: results.comparePassword.id
                    }
                }).then(user => done(null, results.comparePassword))
                    .catch(error => done(error, null));
            }]
        }, (err, results) =>
            callback(err, results.updateLastLogin));
    }

    return {
        add: add,
        login: login,
        find: find
    };
};