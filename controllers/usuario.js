module.exports = function (app) {
    const UserModel = require('../models/usuario');
    const userModel = new UserModel(app);

    function mount(req) {
        let user = {};

        user.nome = req.body.nome;
        user.email = req.body.email;
        user.senha = req.body.senha;
        user.telefones = req.body.telefones;

        return user;
    }

    function indexAction(req, res) {
        userModel.find(req.params.id, (err, result) => responseHandler(err, result, res));
    }

    function createAction(req, res) {
        let user = mount(req);

        userModel.add(user, (err, result) => responseHandler(err, result, res));
    }

    function loginAction(req, res) {
        let user = {
            email: req.body.email,
            senha: req.body.senha
        };

        if (!user.email || !user.senha) {
            return res.status(400).json({
                mensagem: 'O email e a senha devem ser informados.'
            });
        }

        userModel.login(user, (err, result) => responseHandler(err, result, res));
    }

    function responseHandler(err, result, res) {
        let status;

        if (!err) {
            return res.status(200).json(result);
        }

        status = err.status || 500;

        res.status(status).json({
            mensagem: err.message || err
        });
    }

    return {
        indexAction: indexAction,
        createAction: createAction,
        loginAction: loginAction
    };
};