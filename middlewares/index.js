module.exports = function (app) {
    const bodyParser = require('body-parser');
    const allowCors = require('./allowCors');

    app.use(allowCors);
    app.use(bodyParser.json({
        limit: '10mb'
    }));
};