module.exports = function (app) {
    const AuthUtils = require('../utils/auth-utils');
    const jwt = require('jwt-simple');
    const encryptionKey = require('../config/config').auth.encryption_key;

    function checkUserAuth(req, res, next) {
        const Usuario = app.get('models').Usuario;
        let token = req.headers['authorization'];
        let decoded;

        if (!token) {
            return res.status(401).json({
                mensagem: 'Não autorizado.'
            });
        }

        token = token.replace('Bearer ', '');

        Usuario.find({
            where: {
                id: req.params.id
            },
            attributes: ['token', 'ultimo_login']
        }).then(function (data) {
            let isSessionValid;

            if (token !== data.token) {
                return res.status(401).json({
                    mensagem: 'Não autorizado.'
                });
            }

            isSessionValid = AuthUtils.isValidSession(data.ultimo_login);

            if (!isSessionValid) {
                return res.status(401).json({
                    mensagem: 'Sessão inválida.'
                });
            }

            next();
        }).catch((err) => {
            res.status(401).json({
                mensagem: 'Não autorizado.'
            });
        });
    }

    return {
        checkUserAuth: checkUserAuth
    };
};