const jwt = require('jwt-simple');
const bcrypt = require('bcrypt-nodejs');
const jwtSecret = require('../config/config').auth.jwtSecret;
const encryptionKey = require('../config/config').auth.encryption_key;

function encryptPassword(password) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);

    return hash;
}

function isValidPassword(password, hash) {
    return bcrypt.compareSync(password, hash);
}

function getToken(id, lastLogin) {
    let token = jwt.encode({
        id: id,
        ultimo_login: lastLogin
    }, jwtSecret);

    return token;
}

function isValidSession(lastLogin) {
    let now = new Date();
    let diff = (now.getTime() - lastLogin.getTime()) / 60000;

    return !(diff > 30);
}

module.exports = {
    encryptPassword: encryptPassword,
    isValidPassword: isValidPassword,
    getToken: getToken,
    isValidSession: isValidSession
};