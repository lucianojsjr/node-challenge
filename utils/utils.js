module.exports = function () {
    function notFoundHandler(req, res) {
        res.status(404).json({
            mensagem: 'Endpoint não encontrado'
        });
    }

    return {
        notFoundHandler: notFoundHandler
    };
};