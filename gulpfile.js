const gulp = require('gulp');
const jshint = require('gulp-jshint');
let lintFiles = [];

lintFiles.push('models/*.js');
lintFiles.push('!./node_modules/**');

gulp.task('lint', function () {
    gulp.src(lintFiles)
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('jshint-stylish'))
});

gulp.task('watch', function () {
    gulp.watch(lintFiles, ['lint']);
});

gulp.task('default', ['watch']);